<?php
$name = 'consultation';
$display_name = __('Konsultacja','cb');
acf_register_block_type(
    [
        'name' => $name,
        'title' => $display_name,
        'render_template' => 'gb/block/'.$name.'.php',
        'icon' => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path fill="none" d="M0 0h24v24H0V0z" /><path d="M19 13H5v-2h14v2z" /></svg>',
        'category' => 'dev-inv-blocks',
        'supports' => array( 'anchor' => true ),
        'enqueue_assets'	=> function(){
            wp_enqueue_style( 'consultation', get_template_directory_uri().'/gb-css/consultation.css', [],VAR_ASS, 'all');

        },
    ]
);