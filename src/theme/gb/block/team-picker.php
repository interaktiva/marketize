<?php

/**
 * Block template file: gb/block/team-picker.php
 *
 * Team Picker Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'team-picker-' . $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = 'team-picker';
if (!empty($block['className'])) {
  $classes .= ' ' . $block['className'];
}

if (($style = get_field('style')) !== 'std') {
  $classes .= ' team-picker--' . $style;
}

?>
<?php if (have_rows('employers')) : ?>
  <div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($classes); ?>">
    <?php while (have_rows('employers')) {
      the_row();

      $employer = get_sub_field('single');

      if (!$employer) {
        continue;
      }

      global $post;
      $post = $employer;
      get_template_part('parts/blocks/post', 'employer', [ 'style' => $style ]);
      wp_reset_postdata();
    }; ?>

  </div>
<?php endif; ?>