<?php
$id = 'consultation-' . $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = 'consultation';
if (!empty($block['className'])) {
  $classes .= ' ' . $block['className'];
}

$image = get_field('image');

if (!$image) {
  $classes .= ' consultation--no-image';
}

?>

<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($classes); ?>">
  <?php if ($image) { ?>
    <div class="consultation-image image-out-box image-out-box--left">
      <?php echo wp_get_attachment_image($image, 'full', '', ['class' => 'image-out-box__thumbnail']); ?>
    </div>
  <?php } ?>


  <div class="consultation-container">
    <div class="consultation-content">
      <?php if ($content = get_field('content')) : ?>
        <div class="consultation-content__description"><?php echo $content; ?></div>
      <?php endif; ?>

      <?php if ($form = get_field('form')) : ?>

        <div class="consultation-content__form">
          <?php echo do_shortcode($form); ?></div>

      <?php endif; ?>
    </div>
  </div>

</div>