<?php
// Create id attribute allowing for custom "anchor" value.
$id = 'schedule-' . $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = 'schedule';
if (!empty($block['className'])) {
  $classes .= ' ' . $block['className'];
}
?>

<?php if (have_rows('schedule')) : ?>
  <ul id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($classes); ?>">
    <?php while (have_rows('schedule')) : the_row(); ?>
      <li class="schedule__single">
        <?php if ($title = get_sub_field('title')) { ?>
          <strong class="schedule__title"><?php echo esc_html($title); ?></strong>
        <?php }; ?>

        <?php if ($value = get_sub_field('description')) { ?>
          <span class="schedule__value"><?php echo esc_html($value); ?></span>
        <?php }; ?>

      </li>
    <?php endwhile; ?>
  </ul>
<?php endif; ?>