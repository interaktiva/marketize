<?php
// Create id attribute allowing for custom "anchor" value.
$id = 'post-picker-' . $block['id'];
if ( ! empty($block['anchor'] ) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = 'post-picker';
if( ! empty( $block['className'] ) ) {
    $classes .= ' ' . $block['className'];
}
?>

<?php if ( have_rows( 'posts' ) ) : ?>
<div id="<?php echo esc_attr( $id ); ?>" class="<?php echo esc_attr( $classes ); ?>">
<?php while ( have_rows( 'posts' ) ){
  the_row();
  $single = get_sub_field( 'single' );
  if( ! $single ){
    continue;
  }

  global $post;
  $post = $single;
  setup_postdata( $post );
  get_template_part('parts/blocks/post', 'picker');
  wp_reset_postdata( );

};?>

</div>
<?php endif; ?>