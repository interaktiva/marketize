<?php
// Create id attribute allowing for custom "anchor" value.
$id = 'slider-' . $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = 'slider hide-bars-nav';
if (!empty($block['className'])) {
  $classes .= ' ' . $block['className'];
}

if( get_field( 'additional' ) ){
  $classes .= ' slider-has-bar';
}

?>

<?php if (have_rows('slider')) : ?>
  <div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($classes); ?>">
    <div class="slider-list">
      <?php while (have_rows('slider')) : the_row(); ?>
        <div class="slider-single">
          <?php if ($image = get_sub_field('background_image')) {
            echo wp_get_attachment_image($image, [1920, 1080], '', ['class' => 'slider-single__image']);
          }; ?>

          <?php if ($content = get_sub_field('content')) : ?>
            <div class="slider-single__content container"><?php echo wp_kses($content, true); ?></div>
          <?php endif; ?>

        </div>
      <?php endwhile; ?>
    </div>
    <?php if ($additional = get_field('additional')) : ?>
      <div class="slider-bottom container hide-bars-nav"><?php echo $additional; ?></div>
    <?php endif; ?>
  </div>
<?php endif; ?>