<?php
$id = 'separator-' . $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

$classes = 'separator';

if (!empty($block['className'])) {
  $classes .= ' ' . $block['className'];
}

$mobile = get_field('mobile_separator');
$desktop = get_field('desktop_separator');

?>

<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($classes); ?>">
  <?php if ($desktop) : ?>
    <div class="separator-desktop" style="height:<?php echo absint($desktop);?>px;"></div>
  <?php endif; ?>

  <?php if ($mobile) : ?>
    <div class="separator-mobile" style="height:<?php echo absint($mobile);?>px;"></div>
  <?php endif; ?>
</div>