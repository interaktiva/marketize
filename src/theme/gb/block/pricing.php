<?php
// Create id attribute allowing for custom "anchor" value.
$id = 'pricing-' . $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = 'pricing';
if (!empty($block['className'])) {
  $classes .= ' ' . $block['className'];
}
?>

<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($classes); ?>">
  <?php if (have_rows('pricing')) : ?>
    <div class="pricing-list">
      <?php
      while (have_rows('pricing')) : the_row();

        $treatment = get_sub_field( 'zabieg' );
        if( ! $treatment ){
          continue;
        }
        $before = get_field( 'zdjecie_przed', $treatment );
        $after = get_field( 'zdjecie_po', $treatment );
        
        $title = get_the_title($treatment);
        $time = get_field( 'czas', $treatment );
        $price = get_field( 'cena', $treatment );
        $description = get_field( 'opis', $treatment );
        $leader = get_field( 'lekarz', $treatment );
      ?>
        <div class="pricing-single">
          <?php if ($before && $after) : ?>
            <div class="pricing-image compare">
              <div class="compare-block twentytwenty-container">
                <?php
                echo wp_get_attachment_image($before, 'rectangle-medium');
                echo wp_get_attachment_image($after, 'rectangle-medium');
                ?>
              </div>
            </div>
          <?php endif; ?>

          <div class="pricing-content">

            <header class="pricing-header">

              <div class="pricing-header__row">
                <?php if ($title) : ?>
                  <h3 class="pricing-header__title"><?php echo esc_html($title); ?></h3>
                <?php endif; ?>

                <div class="pricing-header__info">
                  <?php if ($time) : ?>
                    <p class="pricing-header__info-time date"><?php echo $time; ?></p>
                  <?php endif; ?>

                  <?php if ($price) : ?>
                    <p class="pricing-header__info-price price"><?php echo $price; ?></p>
                  <?php endif; ?>
                </div>

              </div>

              <?php if ($description) : ?>
                <div class="pricing-header__description"><?php echo $description; ?></div>
              <?php endif; ?>

            </header>

            <div class="pricing-leader">
              <h3 class="pricing-leader__title"><?php _e('Lekarz prowadzący', 'cb'); ?></h3>
              <?php
              global $post;
              $post = $leader;
              setup_postdata($post);
              ?>
              <a class="pricing-leader__single" href="<?php the_permalink();?>">
                <?php the_post_thumbnail([90, 90], ['class' => 'pricing-leader__avatar']); ?>
                <div class="pricing-leader__content">
                  <h4 class="pricing-leader__content-title"><?php the_title(); ?></h4>

                  <?php if ($role = get_field('role', get_the_ID())) : ?>
                    <p class="pricing-leader__content-role"><?php echo esc_html($role); ?></p>
                  <?php endif; ?>

                  <?php if ($postition = get_field('postition', get_the_ID())) : ?>
                    <p class="pricing-leader__content-postition"><?php echo esc_html($postition); ?></p>
                  <?php endif; ?>

                </div>
                  </a>
              <?php wp_reset_postdata(); ?>
            </div>

          </div>

        </div>

      <?php endwhile; ?>
    </div>
  <?php endif; ?>
</div>