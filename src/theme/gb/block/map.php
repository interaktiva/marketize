<?php

// Create id attribute allowing for custom "anchor" value.
$id = 'map-' . $block['id'];
if ( ! empty($block['anchor'] ) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = 'map';
if( ! empty( $block['className'] ) ) {
    $classes .= ' ' . $block['className'];
}

$map = get_field( 'map_type' ) === 'options' ? get_field( 'map', 'option' ) : get_field( 'map_new' );

?>

<div id="<?php echo esc_attr( $id ); ?>" class="<?php echo esc_attr( $classes ); ?>">
	<?php echo $map;?>
</div>