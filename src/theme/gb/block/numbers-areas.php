<?php
// Create id attribute allowing for custom "anchor" value.
$id = 'numbers-areas-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = 'numbers-areas';
if (!empty($block['className'])) {
    $classes .= ' ' . $block['className'];
}
?>
<?php if (have_rows('numbers')) : ?>
    <ul id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($classes); ?>">

        <?php
        while (have_rows('numbers')) : the_row();
            $icon = get_sub_field('icon');
            if (!$icon) {
                continue;
            }
            $title = esc_html(get_sub_field('title'));
            $numbers = esc_html(get_sub_field('numbesr'));
        ?>
            <li class="numbers-areas-single">
                <div class="numbers-areas-single__icon">
                    <?php echo wp_get_attachment_image($icon, 'icon', '', ['class' => 'numbers-areas-single__icon-img']); ?>
                </div>

                <div class="numbers-areas-single__content">
                    <?php if ($numbers) { ?>
                        <strong class="numbers-areas-single__content-number"><?php echo $numbers; ?></strong>
                    <?php }; ?>
                    <?php if ($title) { ?>
                        <span class="numbers-areas-single__content-title"><?php echo $title; ?></span>
                    <?php }; ?>
                </div>
                
            </li>
        <?php endwhile; ?>

    </ul>
<?php endif; ?>