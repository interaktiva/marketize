<?php

// Create id attribute allowing for custom "anchor" value.
$id = 'gallery-' . $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = 'gallery';
if (!empty($block['className'])) {
  $classes .= ' ' . $block['className'];
}

if( get_field( 'type' ) !== 'std' ){
  $classes .= ' gallery--'.esc_attr( get_field( 'type' ) );
}

?>

<?php if (have_rows('gallery')) : ?>
  <div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($classes); ?>">
    <?php
    while (have_rows('gallery')) : the_row();
      $image = get_sub_field('image');
      if (!$image) {
        continue;
      }
    ?>
      <a href="<?php echo wp_get_attachment_image_url($image, 'full'); ?>" class="gallery-single" data-lightbox="<?php echo $id;?>">
      <span class="gallery-single__check"><?php _e( 'Zobacz', 'cb') ;?></span>
        <?php if ($image) {
          echo wp_get_attachment_image($image,  get_row_index() === 1 ? 'full' : 'rectangle-medium', '', ['class' => 'gallery-single__image']);
        }
        ?>
      </a>
    <?php endwhile; ?>
  </div>
<?php endif; ?>