<?php
$id = 'videos-' . $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = 'videos';
if (!empty($block['className'])) {
  $classes .= ' ' . $block['className'];
}
?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($classes); ?>">

  <?php if (have_rows('featured')) : ?>
    <div class="videos-featured">

      <?php
      while (have_rows('featured')) : the_row();
        $image = get_sub_field('image');
        if (!$image) {
          return;
        }

        $url = get_sub_field('url') ? get_sub_field('url') : '#';
      ?>

        <a href="<?php echo esc_url($url); ?>" class="videos-featured__url">
          <?php echo wp_get_attachment_image($image, 'full', '', ['class' => 'videos-featured__image']); ?>
        </a>

      <?php endwhile; ?>
    </div>
  <?php endif; ?>

  <?php if (have_rows('recommended')) : ?>
    <div class="videos-grid">
      <?php
      while (have_rows('recommended')) : the_row();
        $image = get_sub_field('thumbnail');

        if (!$image) {
          continue;
        }

        $link = get_sub_field('link') ? get_sub_field('link') : '#';
      ?>

        <a href="<?php echo esc_url($link); ?>" class="videos-grid__single">
          <?php echo wp_get_attachment_image($image, 'rectangle-medium', '', ['class' => 'videos-featured__image']); ?>
        </a>
      <?php endwhile; ?>
    </div>
  <?php endif; ?>
</div>