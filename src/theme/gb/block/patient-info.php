<?php

$id = 'patient-info-' . $block['id'];
if ( ! empty($block['anchor'] ) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = 'patient-info';
if( ! empty( $block['className'] ) ) {
    $classes .= ' ' . $block['className'];
}

$patient = get_field( 'patient' );

if( ! $patient ){
  return;
}

?>


<div id="<?php echo esc_attr( $id ); ?>" class="<?php echo esc_attr( $classes ); ?>">
	<?php

      global $post;
      $post = $patient;
      setup_postdata( $post );
      get_template_part('parts/blocks/post', 'patient');
      wp_reset_postdata( );

  ?>
</div>