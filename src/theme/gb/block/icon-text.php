<?php
// Create id attribute allowing for custom "anchor" value.
$id = 'icon-list-' . $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = 'icon-list';
if (!empty($block['className'])) {
  $classes .= ' ' . $block['className'];
}
?>

<?php if (have_rows('icon-list')) : ?>
  <ul id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($classes); ?>">
    <?php
    while (have_rows('icon-list')) : the_row();
      $icon = get_sub_field('icon');
      $link = get_sub_field('link') ? esc_url(get_sub_field('link')) : '#';

      if (!$icon) {
        continue;
      }

    ?>
      <li class="icon-list__single">
        <a href="<?php echo $link; ?>" class="icon-list__link">
          <?php echo wp_get_attachment_image($icon, 'icon', '', ['class' => 'icon-list__icon']); ?>
          <span class="icon-list__text"><?php echo esc_html(get_sub_field('text')); ?></span>
        </a>
      </li>
    <?php endwhile; ?>
  </ul>
<?php endif; ?>