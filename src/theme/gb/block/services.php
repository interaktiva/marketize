<?php
$id = 'services-' . $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = 'services';
if (!empty($block['className'])) {
  $classes .= ' ' . $block['className'];
}

if (get_field('style') !== 'style1') {
  $classes .= ' services--' . esc_attr(get_field('style'));
}

?>

<?php if (have_rows('services')) : ?>
  <div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($classes); ?>">
    <?php while (have_rows('services')){
      the_row();
      if( $single = get_sub_field('single') ){
        global $post;
        $post = get_sub_field('single');
        setup_postdata( $post );
        
        get_template_part('parts/blocks/post', 'service');

        wp_reset_postdata( );
      }
      
    }
    ?>

  </div>
<?php endif; ?>