<?php

// Create id attribute allowing for custom "anchor" value.
$id = 'logos-' . $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = 'logos';
if (!empty($block['className'])) {
  $classes .= ' ' . $block['className'];
}

$type = get_field('type');

if ($type !== 'std') {
  $classes .= 'no-slider logos--' . esc_attr(get_field('type'));
}
?>

<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($classes); ?>">
  <?php if (have_rows('logos')) : ?>
    <div class="logos-wrapper">
      <div class="logos-list">
        <?php
        while (have_rows('logos')) : the_row();
          $logo = get_sub_field('logo');
          if (!$logo) {
            continue;
          }
          $link = get_sub_field('link') ? esc_url(get_sub_field('link')) : '#';
        ?>

          <a class="logos-single" href="<?php echo $link; ?>">
            <?php echo wp_get_attachment_image($logo, 'full', '', ['class' => 'logos-single__image']); ?>
          </a>
        <?php endwhile; ?>
      </div>
      <?php if ($type === 'std') : ?>
        <div class="logos-arrows"></div>
      <?php endif; ?>
    </div>
  <?php endif; ?>
</div>