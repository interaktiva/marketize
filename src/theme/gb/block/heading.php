<?php
$classes = 'gb-heading';

if ( ! empty($block['anchor'] ) ) {
    $id = $block['anchor'];
}

if (!empty($block['className'])) {
    $classes .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $classes .= ' align' . $block['align'];
}
$style = '';
$class = [];
if(get_field('font_weight')) array_push($class, get_field('font_weight'));
if(get_field('color')) $style .= 'color: '.get_field('color').';';
if(get_field('line_height')) $style .= 'line-height: '.get_field('line_height').'px;';
if($style) $style = 'style="'.$style.'"';
$class = implode(' ', $class);
if(get_field( 'font_type' ) === "Uppercase") $class .= ' gb-heading--upper';
$headlineType = get_field('heading_type') ?? acf_get_field('heading_type')['default_value'];
?>
<div id="<?php echo esc_attr( $block['id'] ); ?>" class="<?php echo esc_attr($classes); ?>">
    <<?php echo $headlineType; ?> id="<?php echo esc_attr( $block['id'] ); ?>" class="<?php echo $class ?>" <?php echo $style ?>><?php the_field('content') ?></<?php echo $headlineType; ?>>
</div>
