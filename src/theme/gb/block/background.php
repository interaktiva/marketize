<?php
$id = 'background-' . $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

$classes = 'background';
if (!empty($block['className'])) {
  $classes .= ' ' . $block['className'];
}

$default = get_field('background-color') ?? acf_get_field('background-color')['default_value'];

if ($additional = get_field('background-image-color')) $default = $additional;

$style = "background-color:" . $default;
if (get_field('background-image')) {
  $style = 'background-image:url(' . wp_get_attachment_url(get_field('background-image')) . ');';
  $classes .= ' background--image';
}

if(get_field('background-padding') === 'none'){
  $class .= ' background--nopadding';
}

$textColor = get_field( 'text-color' );
?>


<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($classes); ?>" style="color:<?php echo $textColor; ?>">
  <div class="background__background" style="<?php echo $style; ?>"></div>

  <InnerBlocks />
</div>