<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<header id="header" class="header container">
		<div class="header-main">
			<?php echo cb_get_logo('header-logo'); ?>

			<?php if (!is_front_page() && ($visit_page = get_field('visit_page', 'option'))) : ?>

				<a href="<?php echo esc_url($visit_page['url']); ?>" target="<?php echo esc_html($visit_page['target']); ?>" class="rounded-link">
					<?php echo $visit_page['title']; ?>
					<img src="<?php echo get_template_directory_uri(); ?>/img/list-arrow.svg" class="rounded-link__icon">
				</a>

			<?php endif; ?>

			<?php if ($location = get_field('location', 'option')) : ?>
				<p class="location icon-text">
					<img src="<?php echo get_template_directory_uri(); ?>/img/icon-location.svg" class="icon-text__icon">
					<?php echo esc_html($location); ?>
				</p>
			<?php endif; ?>

			<?php if ($phone = get_field('phone_number', 'option')) : ?>
				<a href="<?php echo esc_url($phone['url']); ?>" target="<?php echo $phone['target']; ?>" class="phone-number icon-text">
					<img src="<?php echo get_template_directory_uri(); ?>/img/icon-phone.svg" class="icon-text__icon">
					<?php echo esc_html($phone['title']); ?>
				</a>
			<?php endif; ?>

			<span></span>

			<?php if (has_nav_menu('primary')) { ?>
				<button class="hamburger" role="button">
					<span class="hamburger__box">
						<span class="hamburger__inner"></span>
					</span>
				</button>
			<?php }; ?>

		</div>

		<?php if (has_nav_menu('primary')) { ?>
			<nav class="navigation-box">'
				<button class="navigation-box__exit"><?php _e( 'Zamknij', 'cb' );?></button>
				<div class="navigation-box-container container"><?php bem_menu('primary', 'navigation-box'); ?></div>
			</nav>
		<?php }; ?>


	</header>