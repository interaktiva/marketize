<div class="patient-info-single">
  <h3 class="patient-info-single__title"><?php the_title();?></h3>
  <?php if( $short = get_field( 'short_description', get_the_ID() ) ){?>
    <div class="patient-info-single__content"><?php echo $short;?></div>
  <?php };?>
  <p class="patient-info-single__date date"><?php echo get_the_date('F jS, Y');?></p>
</div>