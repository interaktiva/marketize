<?php

  $title = get_field( 'service_block-title', get_the_ID() ) ? wp_kses( get_field( 'service_block-title', get_the_ID() ), true ) : get_the_title();

?>

<a href="<?php the_permalink();?>" class="services-single">

<?php the_post_thumbnail( 'rectangle-medium', [ 'class' => 'services-single__image' ]  );?>

  <div class="services-content">
    <h3 class="services-content__title"><?php echo $title;?></h3>
  </div>

</a>