<article class="employer-single" id="employer-<?php the_ID(); ?>">
  <?php if (has_post_thumbnail()) : ?>
    <div class="employer-single-image">
      <?php the_post_thumbnail([398, 520], ['class' => 'employer-single-image__thumbnail']); ?>
    </div>
  <?php endif; ?>

  <div class="mobile employer-single-others">
    <h2 class="employer-single-others__title"><?php the_title(); ?></h2>
    <?php if ($role = get_field('role', get_the_ID())) : ?>
      <p class="employer-single-others__role"><?php echo esc_html($role); ?></p>
    <?php endif; ?>

    <?php if ($postition = get_field('position', get_the_ID())) : ?>
      <p class="employer-single-others__position"><?php echo esc_html($postition); ?></p>
    <?php endif; ?>
    
    <?php if ($args['style'] !== 'std') { ?>
    <div class="employer-single-buttons">
      <a href="<?php the_permalink(); ?>" class="button button--more button--bordered"><?php _e('Czytam więcej o ekspercie', 'cb'); ?></a>
      
        <a href="#" class="button button--visit"><?php _e('Chcę umówić się na wizytę', 'cb'); ?></a>

    </div>
    <?php }; ?>

  </div>

  <?php if ($args['style'] === 'std') { ?>
    <div class="mobile employer-single-content">

      <?php if ($description = get_field('short-description', get_the_ID())) : ?>
        <div class="employer-single-content__text"><?php echo $description; ?></div>
      <?php endif; ?>

      <div class="employer-single-buttons">
        <a href="<?php the_permalink(); ?>" class="button button--more button--bordered"><?php _e('Czytam więcej o ekspercie', 'cb'); ?></a>
        <?php if ($args['style'] === 'std') { ?>
          <a href="#" class="button button--visit"><?php _e('Chcę umówić się na wizytę', 'cb'); ?></a>
        <?php }; ?>
      </div>
      

    </div>


  <?php }; ?>


  <div class="employer-single-content desktop">

    <header class="employer-single-header">
      <h2 class="employer-single-header__title"><?php the_title(); ?></h2>
      <?php if ($role = get_field('role', get_the_ID())) : ?>
        <p class="employer-single-header__role"><?php echo esc_html($role); ?></p>
      <?php endif; ?>

      <?php if ($postition = get_field('position', get_the_ID())) : ?>
        <p class="employer-single-header__position"><?php echo esc_html($postition); ?></p>
      <?php endif; ?>
    </header>

    <?php if ($args['style'] === 'std') { ?>

      <?php if ($description = get_field('short-description', get_the_ID())) : ?>
        <div class="employer-single-content__text"><?php echo $description; ?></div>
      <?php endif; ?>

    <?php }; ?>


    <div class="employer-single-buttons">
      <a href="<?php the_permalink(); ?>" class="button button--more button--bordered"><?php _e('Czytam więcej o ekspercie', 'cb'); ?></a>
      <?php if ($args['style'] === 'std') { ?>
        <a href="#" class="button button--visit"><?php _e('Chcę umówić się na wizytę', 'cb'); ?></a>
      <?php }; ?>
    </div>

  </div>
</article>

<?php if (isset($args['style']) &&  $args['style'] === 'std') : ?>
  <div class="employer-divider"></div>
<?php endif; ?>