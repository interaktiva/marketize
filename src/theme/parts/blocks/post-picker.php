<article class="post-picker-single" id="post-picker-single-<?php the_ID(); ?>">
  <div class="post-picker-single__image">
    <?php the_post_thumbnail([512, 350], ['class' => 'post-picker-single__image-thumbnail']); ?>
  </div>
  
  <div class="post-picker-single__content">
    <h3 class="post-picker-single__title"><?php the_title(); ?></h3>
    <?php if (get_the_content()) : ?>
      <div class="post-picker-single__text"><?php echo wp_trim_words(get_the_content(), 40, '...'); ?></div>
    <?php endif; ?>
    <a href="<?php the_permalink(); ?>" class="button button--form button--medium post-picker-single__button"><?php _e('Czytam dalej', 'cb'); ?> </a>
  </div>
</article>