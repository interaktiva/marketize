<article class="page-content<?php if (is_front_page()) echo ' page-content--front'; ?> container">
	<?php if (get_field('heading') !== 'off') : ?>
		<header class="page-content-header">
			<?php
			if (function_exists('yoast_breadcrumb')) {
				yoast_breadcrumb('<p id="breadcrumbs" class="breadcrumbs">', '</p>');
			}

			if( get_field( 'title' ) !== 'hide' ){
			?>
			<h1 class="page-content-header__title">
				<?php if( $role = get_field( 'role-short' ) ){?>
					<span class="page-content-header__title-role"><?php echo esc_html( $role );?></span>
				<?php };?>
				<?php cb_getPageTitle(); ?>
			</h1>
			<?php };?>
		</header>
	<?php endif; ?>
	<?php the_content(); ?>
</article>