<footer class="footer container" id="footer">
	<?php if( is_dynamic_sidebar( 'footer-front' ) ){?>
		<div class="footer-main">
			<?php dynamic_sidebar( 'footer-front' );?>
		</div>
	<?php };?>

	<p class="footer-info">Wszystkie prawa zastrzeżone dla <?php _e( 'Katarzyna Gliwa Stomatologia', 'cb' );?> </p>
</footer>
<?php wp_footer(); ?>
</body>
</html>
