jQuery(document).ready(function() {

  if( $('.logos-list').length ){
    $('.logos-list').each( function(){

      if( $(this).parents( '.logos--gray' ).length ){
        return;
      }

      var that = $(this)

      $(this).slick({
        infinite: false,
        slidesToShow: 4,
        slidesToScroll: 2,
        dots: false,
        arrows: true,
        appendArrows: $( that.siblings('.logos-arrows') ),
        infinite: true,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3,
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
        ]
      })

    } );


        

  }

});