jQuery(document).ready(function() {

  if( $('.slider-list').length ){

    $('.slider-list').slick({
      infinite: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: true,
      arrows: false,
    });
        

  }

});