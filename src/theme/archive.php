<?php get_header(); ?>

<main class="main">
	<article class="page-content container">
		<header class="page-content-header">
			<?php
			if (function_exists('yoast_breadcrumb')) {
				yoast_breadcrumb('<p id="breadcrumbs" class="breadcrumbs">', '</p>');
			}

			if (get_field('title') !== 'hide') {
			?>
				<h1 class="page-content-header__title">
					<?php if ($role = get_field('role-short')) { ?>
						<span class="page-content-header__title-role"><?php echo esc_html($role); ?></span>
					<?php }; ?>
					<?php cb_getPageTitle(); ?>
				</h1>
			<?php }; ?>
		</header>

		<?php if (have_posts()) {
			echo '<div class="post-picker">';
			while (have_posts()) {
				the_post();
			  get_template_part('parts/blocks/post', 'picker');

			}
			echo '</div>';
		}

		?>

	</article>

</main>

<?php get_footer(); ?>