<?php

$footer_sidebar = 'footer';

if ($contact_page = get_field('contact', 'option')) {


	if (is_page($contact_page)) {

		$footer_sidebar .= '-contact';
	}
}

?>

<footer class="footer container" id="footer">
	<?php if (is_dynamic_sidebar($footer_sidebar)) { ?>
		<div class="footer-main">
			<?php dynamic_sidebar($footer_sidebar); ?>
		</div>
	<?php }; ?>

	<p class="footer-info">Wszystkie prawa zastrzeżone dla <?php _e('Katarzyna Gliwa Stomatologia', 'cb'); ?> </p>
</footer>

<?php wp_footer(); ?>
</body>

</html>