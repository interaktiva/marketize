<?php
define('VAR_ASS', 1.0);
include_once('includes/all.php');


add_action('body_class', function ($classes) {

  global $post;
  $blocks = parse_blocks($post->post_content);

  if (!empty($blocks) && $blocks[0]['blockName'] === 'acf/slider') {
    $classes[] = 'has-slider-as-first';
  } else {
    $classes[] = 'header-get-padding';
  }

  if (is_page(get_field('contact', 'option'))) {
    $classes[] = 'contact-page';
  }

  return $classes;
});

add_action('template_redirect', function () {

  if (is_post_type_archive('zabiegi')) {

    wp_safe_redirect(get_field('treatments', 'option'));
    exit;
  }
  

  if (is_post_type_archive('uslugi')) {

    wp_safe_redirect(get_field('service', 'option'));
    exit;
  }
});

add_filter('wpseo_breadcrumb_links', 'wpse_100012_override_yoast_breadcrumb_trail');

function wpse_100012_override_yoast_breadcrumb_trail($links)
{
  global $post;

  if (is_singular('uslugi')) {
    $breadcrumb[] = array(
      'url' => get_permalink(get_field('service', 'option')),
      'text' => __('Leczenie', 'cb'),
    );

    array_splice($links, 1, -2, $breadcrumb);
  }else if (is_singular('pracownicy')) {
    $breadcrumb[] = array(
      'url' => get_permalink(get_field('about', 'option')),
      'text' => __('O nas', 'cb'),
    );

    array_splice($links, 1, -2, $breadcrumb);
  }

  return $links;
}


add_filter('template_include', 'archive_template_change', 99);

function archive_template_change($template)
{ 
  if (is_archive() || is_home() || is_tax()) {
    $template = locate_template('archive.php');
  }

  return $template;
}
