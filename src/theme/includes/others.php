<?php

function custom_excerpt_length()
{
	return 22;
}

add_filter('excerpt_length', 'custom_excerpt_length');

function is_search_has_results()
{
	return 0 != $GLOBALS['wp_query']->found_posts;
}


add_filter('wpcf7_autop_or_not', '__return_false');


add_action('wp_head', function () {

	if ($fb_head = get_field('fb_box_header', 'option')) {
		echo $fb_head;
	}
}, 9999);

add_action('wp_footer', function () {

	if ($fb_footer = get_field('fb_box_footer', 'option')) { ?>

		<div class="fb-box">
			<div class="fb-box__icon">
				<img src="<?php echo get_template_directory_uri(); ?>/img/icon-fb.svg" alt="" class="fb-box__icon-img">
			</div>
			<div class="fb-box__box"><?php echo $fb_footer; ?></div>
		</div>

	<?php }
}, 9999);

function marketize_counters()
{ ?>

	<?php if (have_rows('counters', 'option')) : ?>
		<ul class="counters">
			<?php while (have_rows('counters', 'option')) : the_row(); ?>
				<li class="counters__single">
					<?php if ($icon = get_sub_field('icon')) {
						echo wp_get_attachment_image($icon, 'icon', '', ['class' => 'counters__icon']);
					}; ?>
					<div class="counters__content">
						<?php if ($number = get_sub_field('number')) { ?>
							<strong class="counters__content-count"><?php echo esc_html($number); ?></strong>
						<?php }; ?>

						<?php if ($title = get_sub_field('title')) { ?>
							<span class="counters__content-title"><?php echo esc_html($title); ?></span>
						<?php }; ?>
					</div>
				</li>
			<?php endwhile; ?>
		</ul>
	<?php endif; ?>

<?php }

add_action('wp_footer', 'marketize_counters', 20);


function marketize_expert_modal()
{

	global $post;

	$blocks = parse_blocks($post->post_content);
	$meets = get_field('cf_meets', 'option');
	if (!$meets || ! array_search('acf/team-picker', array_column($blocks, 'blockName'))) {
		return;
	}
?>

	<div class="modal">
		<div class="modal-content">

			<header class="modal-content__header">
				<h3 class="modal-content__title"><?php _e('Spotkanie', 'cb'); ?></h3>
				<button class="modal-content__header-exit"><?php _e('Zamknij', 'cb'); ?></button>
			</header>

			<div class="modal-content__form"><?php echo do_shortcode($meets); ?></div>

		</div>
	</div>

<?php
}

add_action('wp_footer', 'marketize_expert_modal', 30);


function cb_phone( $atts ) {
	$phone = shortcode_atts( [
		'area_code' => '+48',
		'number' => ($phone = get_field( 'phone_number', 'option' )) ? $phone['url'] : ''
	], $atts);
	


	return '<a href="'.$phone['number'].'">'.$phone['area_code'].' '.str_replace('tel:','',$phone['number']).'</a>';

}
add_shortcode( 'numer_telefonu', 'cb_phone' );


function cb_email( $atts ) {
	$email = shortcode_atts( [
		'email' => ($email = get_field( 'email', 'option' )) ? $email : ''
	], $atts);
	


	return '<a href="mailto:'.$email['email'].'">'.$email['email'].'</a>';

}
add_shortcode( 'adres_email', 'cb_email' );