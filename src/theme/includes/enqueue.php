<?php
function cb_resources()
{

	// Register

	wp_register_style( 'slick', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.css', [], VAR_ASS );
	wp_register_script( 'slick', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js', [], VAR_ASS, true );
	wp_register_style( 'lightbox', 'https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/css/lightbox.min.css', [], VAR_ASS );
	wp_register_script( 'lightbox', 'https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/js/lightbox.min.js', [], VAR_ASS, true );

	wp_enqueue_script('header_js', get_template_directory_uri() . '/js/header-bundle.js', null, VAR_ASS, false);
	wp_enqueue_script('footer_js', get_template_directory_uri() . '/js/footer-bundle.js', null, VAR_ASS, true);


	wp_enqueue_style('style', get_stylesheet_uri());


}

add_action('wp_enqueue_scripts', 'cb_resources', 9999999);

function marketize_load_css() {
	
	wp_register_style( 'admin_css', get_template_directory_uri() . '/css/admin.css', false, '1.0.0' );
	wp_enqueue_style( 'admin_css' );

}

add_action( 'admin_enqueue_scripts', 'marketize_load_css' );