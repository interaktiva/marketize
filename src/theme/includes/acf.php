<?php

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Ustawienia szablonu',
		'menu_title'	=> 'Ustawienia szablonu',
		'menu_slug' 	=> 'theme-general-settings',
	));
	
}

?>