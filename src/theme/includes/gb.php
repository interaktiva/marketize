<?php
function cb_acf_init()
{

  if (function_exists('acf_register_block')) {
    foreach (glob(get_template_directory() . "/gb/init/*.php") as $filename) {
      include $filename;
    }
  }
}

add_action('acf/init', 'cb_acf_init');

function cb_acf_category($categories, $post)
{
  return array_merge(
    $categories,
    [
      [
        'slug' => 'cb-blocks',
        'title' => __('Coolbrand', 'cb'),
        'icon' => 'welcome-widgets-menus'
      ]
    ]
  );
}

add_filter('block_categories', 'cb_acf_category', 10, 2);
