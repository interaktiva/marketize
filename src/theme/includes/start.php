<?php

function cb_setup() {
	// Handle Titles
	add_theme_support( 'title-tag' );

	// Add featured image support
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'small-thumbnail', 720, 720, true );
	add_image_size( 'rectangle-small', 340, 180, true );
	add_image_size( 'rectangle-medium', 650, 500, true );
	
	add_image_size( 'icon', 185, 185, false );

	register_nav_menus([
		'primary' => __('Menu główne','cb')
	]);
}
add_action( 'after_setup_theme', 'cb_setup' );

function cis_gutenberg_assets() {

	wp_enqueue_script(
		'cis-gutenberg', 
		get_template_directory_uri() . '/js/gutenberg.js', 
		array( 'wp-blocks', 'wp-dom' ), 
		VAR_ASS,
		true
	);
}
add_action( 'enqueue_block_editor_assets', 'cis_gutenberg_assets' );

show_admin_bar(false);
?>