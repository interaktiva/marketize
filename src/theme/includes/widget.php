<?php
function cb_widgets() {

	register_sidebar(
		array(
			'name'          => __( 'Stopka', 'cb' ),
			'id'            => 'footer',
			'before_widget' => '<div class="footer__item" id="%1$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h2 class="footer__title">',
			'after_title'   => '</h2>',
		)
	);

	register_sidebar(
		array(
			'name'          => __( 'Stopka strony głównej', 'cb' ),
			'id'            => 'footer-front',
			'before_widget' => '<div class="footer__item" id="%1$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h2 class="footer__title">',
			'after_title'   => '</h2>',
		)
	);

	register_sidebar(
		array(
			'name'          => __( 'Stopka strony kontaktowej', 'cb' ),
			'id'            => 'footer-contact',
			'before_widget' => '<div class="footer__item" id="%1$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h2 class="footer__title">',
			'after_title'   => '</h2>',
		)
	);

}

add_action( 'widgets_init', 'cb_widgets' );
?>