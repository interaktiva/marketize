<?php

function cb_get_logo($classes = '')
{
  $class = 'logo';
  if ($classes) $class .= ' ' . $classes;

  ob_start(); ?>

  <a href="<?php echo get_home_url(); ?>" class="<?php echo $class; ?>">
    <?php if ($logotyp = get_field('logotyp', 'option')) : ?>
      <img src="<?php echo $logotyp['url']; ?>" alt="<?php echo $logotyp['alt']; ?>" width="199" height="54" class="logo__image">
    <?php else : ?>
      <img src="<?php echo get_template_directory_uri(); ?>/img/logo.svg" width="199" height="54" class="logo__image">
    <?php endif; ?>
  </a>


<?php
  $content = ob_get_contents();
  ob_end_clean();

  return $content;
}

function cb_return_no_accents_word($word)
{
  return strtolower(remove_accents(str_replace(array('\'', '"', ',', ';', '<', '>', '(', ')', ' '), '', esc_html($word))));
}


function cb_getPageTitle()
{
  if (is_home()) {
    if (get_option('page_for_posts', true)) {
      echo get_the_title(get_option('page_for_posts', true));
    } else {
      _e('Ostatnie wpisy', 'cie');
    }
  } elseif (is_archive()) {
    $term = get_term_by('slug', get_query_var('term'), get_query_var('taxonomy'));
    if ($term) {
      echo $term->name;
    } elseif (is_post_type_archive()) {
      echo get_queried_object()->labels->name;
    } elseif (is_day()) {
      printf(__('Codzienne archiwa: %s', 'cie'), get_the_date());
    } elseif (is_month()) {
      printf(__('Miesięczne archiwa: %s', 'cie'), get_the_date('F Y'));
    } elseif (is_year()) {
      printf(__('Roczne archwia: %s', 'cie'), get_the_date('Y'));
    } elseif (is_author()) {
      $author = get_queried_object();
      printf(__('Autorzy archiwów: %s', 'cie'), $author->display_name);
    } else {
      single_cat_title();
    }
  } elseif (is_search()) {
    printf(__('Wyniki wyszukiwania dla: <strong>%s</strong>', 'cie'), get_search_query());
  } elseif (is_404()) {
    _e('Brak wyników', 'cie');
  } elseif (is_singular('praca')) {
    _e('Praca', 'cie');
  } else {
    the_title();
  }
}


function cb_has_children($page_id)
{
  $children = get_children(array(
    'post_parent' => $page_id,
    'post_type'   => 'page',
    'post_status' => 'publish'
  ));

  return (is_array($children) && !empty($children)) ? true : false;
}
