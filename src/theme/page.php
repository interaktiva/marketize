<?php get_header(); ?>

<main class="main">

	<?php if (have_posts()) {
		while (have_posts()) {
			the_post();
			get_template_part('parts/content/content');
		}
	}
	
	?>
	
</main>

<?php get_footer(); ?>